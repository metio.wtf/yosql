name: Verify Commits
on:
  push:
    branches: [ main ]
  pull_request:
    branches: [ main ]
env:
  JAVA_VERSION: 17
  GRAAL_VERSION: latest
jobs:
  maven:
    name: Build Maven Tooling on ${{ matrix.os }}
    runs-on: ${{ matrix.os }}
    strategy:
      matrix:
        os:
          - ubuntu-latest
          - macos-latest
          - windows-latest
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v3
      - id: graal
        name: Setup GraalVM
        uses: graalvm/setup-graalvm@v1
        with:
          version: ${{ env.GRAAL_VERSION }}
          java-version: ${{ env.JAVA_VERSION }}
          components: native-image
          github-token: ${{ secrets.GITHUB_TOKEN }}
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v3
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: maven
        name: Maven Tooling
        run: mvn --batch-mode --projects yosql-tooling/yosql-tooling-maven,yosql-examples/yosql-examples-common --also-make install
      - id: maven_examples
        name: Maven Examples
        run: mvn --batch-mode --projects yosql-examples/yosql-examples-maven --also-make --also-make-dependents verify
  gradle:
    name: Build Gradle Tooling on ${{ matrix.os }}
    runs-on: ${{ matrix.os }}
    strategy:
      matrix:
        os:
          - ubuntu-latest
          - macos-latest
          - windows-latest
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v3
      - id: graal
        name: Setup GraalVM
        uses: graalvm/setup-graalvm@v1
        with:
          version: ${{ env.GRAAL_VERSION }}
          java-version: ${{ env.JAVA_VERSION }}
          components: native-image
          github-token: ${{ secrets.GITHUB_TOKEN }}
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v3
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: maven
        name: Maven Tooling
        run: mvn --batch-mode --projects yosql-tooling/yosql-tooling-dagger,yosql-examples/yosql-examples-common --also-make install
      - id: gradle
        name: Gradle Tooling
        run: cd ./yosql-examples/yosql-examples-gradle/ && ./gradlew build run
  ant:
    name: Build Ant Tooling on ${{ matrix.os }}
    runs-on: ${{ matrix.os }}
    strategy:
      matrix:
        os:
          - ubuntu-latest
          - macos-latest
          - windows-latest
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v3
      - id: graal
        name: Setup GraalVM
        uses: graalvm/setup-graalvm@v1
        with:
          version: ${{ env.GRAAL_VERSION }}
          java-version: ${{ env.JAVA_VERSION }}
          components: native-image
          github-token: ${{ secrets.GITHUB_TOKEN }}
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v3
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: maven
        name: Ant Tooling
        run: mvn --batch-mode --projects yosql-tooling/yosql-tooling-ant --also-make verify
      - id: maven_examples
        name: Ant Examples
        run: mvn --batch-mode --projects yosql-examples/yosql-examples-ant --also-make --also-make-dependents verify
      - id: upload-ant-tooling
        name: Upload Ant Tooling
        uses: actions/upload-artifact@v3
        with:
          name: yosql-tooling-ant
          path: ./yosql-tooling/yosql-tooling-ant/target/yosql-tooling-ant-*-dist*
  cli:
    name: Build CLI Tooling on ${{ matrix.os }}
    runs-on: ${{ matrix.os }}
    strategy:
      matrix:
        os:
          - ubuntu-latest
          - macos-latest
          - windows-latest
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v3
      - id: graal
        name: Setup GraalVM
        uses: graalvm/setup-graalvm@v1
        with:
          version: ${{ env.GRAAL_VERSION }}
          java-version: ${{ env.JAVA_VERSION }}
          components: native-image
          github-token: ${{ secrets.GITHUB_TOKEN }}
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v3
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: maven
        name: CLI Tooling
        run: mvn --batch-mode --projects yosql-tooling/yosql-tooling-cli --also-make --define skipNativeBuild=false verify
      - id: maven_examples
        name: CLI Examples
        run: mvn --batch-mode --projects yosql-examples/yosql-examples-cli --also-make --also-make-dependents verify
      - id: upload-jvm-cli-tooling
        name: Upload JVM CLI Tooling
        uses: actions/upload-artifact@v3
        if: runner.os == 'Linux'
        with:
          name: yosql-tooling-cli-jvm
          path: ./yosql-tooling/yosql-tooling-cli/target/yosql-tooling-cli-*-jvm*
      - id: upload-linux-cli-tooling
        name: Upload Linux CLI Tooling
        uses: actions/upload-artifact@v3
        if: runner.os == 'Linux'
        with:
          name: yosql-tooling-cli-linux
          path: ./yosql-tooling/yosql-tooling-cli/target/yosql-tooling-cli-*-linux*
      - id: upload-mac-cli-tooling
        name: Upload Mac OSX CLI Tooling
        uses: actions/upload-artifact@v3
        if: runner.os == 'macOS'
        with:
          name: yosql-tooling-cli-mac
          path: ./yosql-tooling/yosql-tooling-cli/target/yosql-tooling-cli-*-mac*
      - id: upload-windows-cli-tooling
        name: Upload Windows CLI Tooling
        uses: actions/upload-artifact@v3
        if: runner.os == 'Windows'
        with:
          name: yosql-tooling-cli-windows
          path: ./yosql-tooling/yosql-tooling-cli/target/yosql-tooling-cli-*-windows*
  benchmarks:
    name: Build Benchmarks on ${{ matrix.os }}
    runs-on: ${{ matrix.os }}
    strategy:
      matrix:
        os:
          - ubuntu-latest
          - macos-latest
          - windows-latest
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v3
      - id: graal
        name: Setup GraalVM
        uses: graalvm/setup-graalvm@v1
        with:
          version: ${{ env.GRAAL_VERSION }}
          java-version: ${{ env.JAVA_VERSION }}
          components: native-image
          github-token: ${{ secrets.GITHUB_TOKEN }}
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v3
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: maven
        name: Maven Tooling
        run: mvn --batch-mode --projects yosql-tooling/yosql-tooling-maven --also-make install
      - id: maven_benchmarks
        name: Build Benchmarks
        run: mvn --batch-mode --projects yosql-benchmarks --also-make --also-make-dependents verify
  website:
    name: Build Website on ${{ matrix.os }}
    runs-on: ${{ matrix.os }}
    strategy:
      matrix:
        os:
          - ubuntu-latest
#          - macos-latest # TODO: enable once https://github.com/peaceiris/actions-hugo/issues/605 is fixed
#          - windows-latest # TODO: enable once https://github.com/peaceiris/actions-hugo/issues/608 is fixed
    steps:
      - id: checkout
        name: Clone Git Repository
        uses: actions/checkout@v3
      - id: hugo
        name: Setup Hugo
        uses: peaceiris/actions-hugo@v2
        with:
          hugo-version: latest
      - id: graal
        name: Setup GraalVM
        uses: graalvm/setup-graalvm@v1
        with:
          version: ${{ env.GRAAL_VERSION }}
          java-version: ${{ env.JAVA_VERSION }}
          components: native-image
          github-token: ${{ secrets.GITHUB_TOKEN }}
      - id: cache
        name: Cache Maven Repository
        uses: actions/cache@v3
        with:
          path: ~/.m2/repository
          key: ${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}
          restore-keys: |
            ${{ runner.os }}-maven-
      - id: maven
        name: Maven Tooling
        run: mvn --batch-mode --projects yosql-website --also-make --define skipTests install
      - id: website
        name: Build Website
        run: hugo --minify --printI18nWarnings --printPathWarnings --printUnusedTemplates --source yosql-website
        env:
          YOSQL_RELEASE: 0.0.0
      - id: htmltest
        name: Run htmltest
        uses: wjdp/htmltest-action@master
        with:
          config: ./yosql-website/htmltest.yml
